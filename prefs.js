'use strict';

const { Gtk, GObject, Gio } = imports.gi;

const Me = imports.misc.extensionUtils.getCurrentExtension();

function init () {
}

 //get settings from schemas file
 function getSettings() {
     const GioSSS = Gio.SettingsSchemaSource;
     const schemaSource = GioSSS.new_from_directory(
         Me.dir.get_child('schemas').get_path(),
         GioSSS.get_default(),
         false
     );  
     const schemaObj = schemaSource.lookup(
     'org.gnome.shell.extensions.YubIcon', true);
     if (!schemaObj) {
         throw new Error('cannot find schemas');
     }   
     return new Gio.Settings({ settings_schema : schemaObj }); 
 }

//create widget as defined in PrefsWidget
function buildPrefsWidget () {
    return new PrefsWidget();
}

//widget class
let PrefsWidget = GObject.registerClass (
class PrefsWidget extends Gtk.ScrolledWindow {
    _init() {
        super._init({
            hscrollbar_policy: Gtk.PolicyType.NEVER,
        });

    //Get Settings
    const settings = getSettings();
    
    //Add ListBox Element
    let listbox = new Gtk.ListBox({
    	selection_mode: Gtk.SelectionMode.NONE,
        show_separators: true,
        halign: Gtk.Align.CENTER,
        valign: Gtk.Align.START,
        hexpand: true,
        margin_start: 60,
        margin_end: 60,
        margin_top: 60,
        margin_bottom: 60,
    });
    listbox.get_style_context().add_class('frame');
    this.set_child(listbox);
    
    //Add ListBoxRow and Box
    let auth_row = new Gtk.ListBoxRow();
    listbox.insert(auth_row, 0);
    
    let auth_box = new Gtk.Box({
        spacing: 12,
        margin_start: 12,
        margin_end: 12,
        margin_top: 12,
        margin_bottom: 12,
    });
    auth_row.set_child(auth_box);
    
    //display label
    let auth_label = new Gtk.Label({
    	label: 'Show Auth Menu',
    	xalign: 0,
    	hexpand: true,
    	width_chars: 25,
    });
    auth_box.append(auth_label, false, false, 0)


    //set auth_switch to the the saved status
    let auth_switch = new Gtk.Switch();
    let display_auth = settings.get_boolean('display-auth');
    auth_switch.set_active(display_auth);
	auth_switch.connect('state-set', () => {
		log( 'YubIconPrefs: Authentication options switched to: ' + auth_switch.get_active() );
		settings.set_boolean('display-auth', auth_switch.get_active() );
	});
    auth_box.append(auth_switch, false, false, 0)
    
	//Add ListBoxRow and Box
    let sudo_row = new Gtk.ListBoxRow();
    listbox.insert(sudo_row, 1);
    
    let sudo_box = new Gtk.Box({
        spacing: 12,
        margin_start: 12,
        margin_end: 12,
        margin_top: 12,
        margin_bottom: 12,
    });
    sudo_row.set_child(sudo_box);
    
    //display label
    let sudo_label = new Gtk.Label({
    	label: 'Show Sudo Menu',
    	xalign: 0,
    	hexpand: true,
    	width_chars: 25,
    });
    sudo_box.append(sudo_label, false, false, 0)


    //set auth_switch to the the saved status
    let sudo_switch = new Gtk.Switch();
    let display_sudo = settings.get_boolean('display-sudo');
    sudo_switch.set_active(display_sudo);
	sudo_switch.connect('state-set', () => {
		log( 'YubIconPrefs: Sudo options switched to: ' + sudo_switch.get_active() );
		settings.set_boolean('display-sudo', sudo_switch.get_active() );
	});
   sudo_box.append(sudo_switch, false, false, 0)
    
    //Add ListBoxRow and Box
    let gdm_row = new Gtk.ListBoxRow();
    listbox.insert(gdm_row, 2);
    
    let gdm_box = new Gtk.Box({
        spacing: 12,
        margin_start: 12,
        margin_end: 12,
        margin_top: 12,
        margin_bottom: 12,
    });
    gdm_row.set_child(gdm_box);

    //display label
    let gdm_label = new Gtk.Label({
    	label: 'Show Gdm Menu',
    	xalign: 0,
    	hexpand: true,
    	width_chars: 25,
    });
    gdm_box.append(gdm_label, false, false, 0)

    //set gdm_switch to the the saved status
    let gdm_switch = new Gtk.Switch();
    let display_Gdm = settings.get_boolean('display-gdm');
    gdm_switch.set_active(display_Gdm);
	gdm_switch.connect('state-set', () => {
		log( 'YubIconPrefs: GDM options switched to: ' + gdm_switch.get_active() );
		settings.set_boolean('display-gdm', gdm_switch.get_active() );
	});
    gdm_box.append(gdm_switch, false, false, 0)

    //Add ListBoxRow and Box
    let tools_row = new Gtk.ListBoxRow();
    listbox.insert(tools_row, 3);
    
    let tools_box = new Gtk.Box({
        spacing: 12,
        margin_start: 12,
        margin_end: 12,
        margin_top: 12,
        margin_bottom: 12,
    });
    tools_row.set_child(tools_box);

    //display label
    let tools_label = new Gtk.Label({
    	label: 'Show Tools Menu',
    	xalign: 0,
    	hexpand: true,
    	width_chars: 25,
    });
    tools_box.append(tools_label, false, false, 0)

    //set tools_switch to the the saved status
    let tools_switch = new Gtk.Switch();
    let display_Tool = settings.get_boolean('display-tools');
    tools_switch.set_active(display_Tool);
	tools_switch.connect('state-set', () => {
        log( 'YubIconPrefs: External Tools options switched to: ' + tools_switch.get_active() );
        settings.set_boolean('display-tools', tools_switch.get_active() );
	});
    tools_box.append(tools_switch, false, false, 0)

    }

});
