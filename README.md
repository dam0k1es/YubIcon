# YubIcon

*ARCHIVED*

This repository is no longer actively maintained as I no longer use the Gnome shell!

Gnome Shell Extension for Yubico's Yubikey

With YubIcon you can change the configuration in /etc/pam.d \
to "required" or "sufficient" so that you may use a YubiKey \
to authenticate. The files I used are "system-auth" on Arch \
and Fedora, "common-auth" on Debian and "sudo" and \
"gdm-password" on all of them. The extension also allows you \
to launch "KeePassXC", "Yubico Authenticator" and the \
"Yubikey Personalization Tool" if one of them is installed.

![YubIcon.png](https://gitlab.com/dam0k1es/YubIcon/-/raw/main/img/extension.png)

BUGS: 

Login in with a YubiKey does not unlock the Gnome-Keyring, \
This is not caused by the extension, it's caused by the way \
the Keyring is unlocked.

Fedora does not seem to care about "gdm-password". 

RESSOURCES\
https://developers.yubico.com/yubico-pam/ or https://github.com/Yubico/yubico-pam \
https://wiki.archlinux.org/index.php/Yubikey \
https://support.yubico.com/hc/en-us/articles/360016649099-Ubuntu-Linux-Login-Guide-U2F
